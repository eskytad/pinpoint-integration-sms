const AWS = require('aws-sdk');

export class SMSService {
  // private aws_region:string = "us-east-1";
  // private originationNumber: string = "+12065550199";
  // The phone number or short code to send the message from. The phone number
  // or short code that you specify has to be associated with your Amazon Pinpoint
  // account. For best results, specify long codes in E.164 format.
  // private destinationNumber: string = "+1703-474-5962";
  /* private message: string = "This message was sent through Amazon Pinpoint "
   + "using the AWS SDK for JavaScript in Node.js. Reply STOP to "
   + "opt out."; */
  // private message: string = "Pinpoint sms test";
  // private applicationId = "f8a1856ce3b7482c90a3d39df128cec9";
  // private messageType: string = "TRANSACTIONAL";
  // private registeredKeyword:string = "myKeyword";
  // private senderId:string = "MySenderID";
  private pinpoint: any;
  private originationNumber: string = process.env.ORIGINATION_NUMBER;

  constructor() {
    this.pinpoint = new AWS.Pinpoint({ region: process.env.region });
  }
  public async postSMS(event: any): Promise<any> {
    const params: any = {
      ApplicationId: event.appId,
      MessageRequest: {
        Addresses: {
          [event.destinationNumber]: {
            ChannelType: 'SMS'
          }
        },
        MessageConfiguration: {
          SMSMessage: {
            Body: event.message,
            MessageType: event.messageType,
          }
        }
      }
    };

    const response = await new Promise((resolve: any, reject: any) => {
      this.pinpoint.sendMessages(params, function (err, res) {
        if (err) {
          console.log(err.message);
          return reject(err);
        } else {
          console.log(JSON.stringify(res));
          console.log("Message sent! " + res.MessageResponse.Result[event.destinationNumber].StatusMessage);
          return resolve(res);
        }
      })
    });
    return response;
  }

  public async validateNumber(event: any): Promise<any> {
    let destinationNumber: String = event.destinationNumber;
    if (destinationNumber.length == 10) {
      destinationNumber = "+1" + destinationNumber;
    }
    const params = {
      NumberValidateRequest: {
        IsoCountryCode: 'US',
        PhoneNumber: destinationNumber
      }
    };
    const response = await new Promise((resolve: any, reject: any) => {
      this.pinpoint.phoneNumberValidate(params, function (err, data) {
        if (err) {
          console.log(err, err.stack);
          return reject(err);
        } else {
          console.log(JSON.stringify(data));
          //return data;
          if (data['NumberValidateResponse']['PhoneTypeCode'] == 0) {
            return resolve(data);
          } else {
            const error: any = {
              status: 400,
              message: "Received a phone number that isn't capable of receiving SMS messages. No endpoint created.",
            };
            console.log("Received a phone number that isn't capable of receiving SMS messages. No endpoint created.");
            return reject(error);
          }
        }
      })
    });
    return response;
  }

  public async createEndpoint(event: any): Promise<any> {
    const destinationNumber = event.validatedNumData['NumberValidateResponse']['CleansedPhoneNumberE164'];
    const endpointId = event.validatedNumData['NumberValidateResponse']['CleansedPhoneNumberE164'].substring(1);
    const params: any = {
      ApplicationId: event.appId,
      // The Endpoint ID is equal to the cleansed phone number minus the leading
      // plus sign. This makes it easier to easily update the endpoint later.
      EndpointId: endpointId,
      EndpointRequest: {
        ChannelType: 'SMS',
        Address: destinationNumber,
        // OptOut is set to ALL (that is, endpoint is opted out of all messages)
        // because the recipient hasn't confirmed their subscription at this
        // point. When they confirm, a different Lambda function changes this 
        // value to NONE (not opted out).
        OptOut: 'ALL',
        Location: {
          PostalCode: event.validatedNumData['NumberValidateResponse']['ZipCode'],
          City: event.validatedNumData['NumberValidateResponse']['City'],
          Country: event.validatedNumData['NumberValidateResponse']['CountryCodeIso2'],
        },
        Demographic: {
          Timezone: event.validatedNumData['NumberValidateResponse']['Timezone']
        },
        Attributes: {
          Source: [
            event.source
          ]
        },
        User: {
          UserAttributes: {
            FirstName: [
              event.firstName
            ],
            LastName: [
              event.lastName
            ]
          }
        }
      }
    };
    const response = await new Promise((resolve: any, reject: any) => {
      this.pinpoint.updateEndpoint(params, function (err, data) {
        if (err) {
          console.log(err, err.stack);
          return reject(err);
        } else {
          console.log(data);
          // this.postSMS(destinationNumber);
          return resolve(data);
        }
      })
    });
    return response;
  }

  public async updateEndpoint(event: any): Promise<any> {
    const endpointId = event.originationNumber.substring(1);

    const params: any = {
      ApplicationId: event.appId,
      EndpointId: endpointId,
      EndpointRequest: {
        Address: this.originationNumber,
        ChannelType: 'SMS',
        OptOut: 'NONE',
        Attributes: {
          OptInTimestamp: [
            event.timestamp
          ]
        },
      }
    };
    const response = await new Promise((resolve: any, reject: any) => {
      this.pinpoint.updateEndpoint(params, function (err, data) {
        if (err) {
          console.log("An error occurred.\n");
          console.log(err, err.stack);
          return reject(err);
        }
        else {
          console.log("Successfully changed the opt status of endpoint ID " + endpointId);
          return resolve(data);
        }
      });
    });
    return response;
  }

}


