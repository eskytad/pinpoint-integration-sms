import { SMSService } from '../../service/sms-service';
const express = require('express');
export const app = express();
const serverless = require('serverless-http');

export const handler = serverless(app, {
  request: async function (request: any) {
  },
});

app.use('/', async (req: any, res: any) => {
  // console.log('Request: ', req.params);
  let event = Buffer.isBuffer(req.body) ? JSON.parse(req.body.toString()) : req.body;
  let status: number = 200;
  let createEndPointResponse: any = null;
  try {
    const valNumData = await new SMSService().validateNumber(event);
    if (valNumData['NumberValidateResponse']['PhoneTypeCode'] == 0) {
      event.validatedNumData = valNumData;
      createEndPointResponse = await new SMSService().createEndpoint(event);
    }
    res.status(status).send(createEndPointResponse);
  } catch (err) {
    res.status(500).send(err);
  }
});
