import { SMSService } from '../../service/sms-service';
const express = require('express');
export const app = express();
const serverless = require('serverless-http');

export const handler = serverless(app, {
  request: async function (request: any) {
  },
});

app.use('/', async (req: any, res: any) => {
  // console.log('Request: ', req.params);
  const event = Buffer.isBuffer(req.body) ? JSON.parse(req.body.toString()) : req.body;
  let status: number = 200;
  let smsRespone: any = null;
  try {
    smsRespone = await new SMSService().postSMS(event);
    status = smsRespone.MessageResponse.Result[event.destinationNumber].StatusCode || status;
    res.status(status).send(smsRespone);
  } catch (err) {
    res.status(500).send(err);
  }
});
