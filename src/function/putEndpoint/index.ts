import { SMSService } from '../../service/sms-service';
const express = require('express');
export const app = express();
const serverless = require('serverless-http');

export const handler = serverless(app, {
  request: async function (request: any) {
  },
});

app.use('/', async (req: any, res: any) => {
  // console.log('Request: ', req.params);
  const event = Buffer.isBuffer(req.body) ? JSON.parse(req.body.toString()) : req.body;
  let status: number = 200;
  let updateEndpointResponse: any = null;
  const timestamp = event.Records[0].Sns.Timestamp;
  const message = JSON.parse(event.Records[0].Sns.Message);
  const originationNumber = message.originationNumber;
  const response = message.messageBody.toLowerCase();
  const confirmKeyword = 'yes';
  event.timestamp = timestamp;
  event.originationNumber = originationNumber;
  
  try {
    if (response.includes(confirmKeyword)) {
      updateEndpointResponse = await new SMSService().updateEndpoint(event);
    }
    res.status(status).send(updateEndpointResponse);
  } catch (err) {
    res.status(500).send(err);
  }
});
