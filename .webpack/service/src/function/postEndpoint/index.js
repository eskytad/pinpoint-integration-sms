(function(e, a) { for(var i in a) e[i] = a[i]; }(exports, /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/function/postEndpoint/index.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/function/postEndpoint/index.ts":
/*!********************************************!*\
  !*** ./src/function/postEndpoint/index.ts ***!
  \********************************************/
/*! exports provided: app, handler */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"app\", function() { return app; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"handler\", function() { return handler; });\n/* harmony import */ var _service_sms_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../service/sms-service */ \"./src/service/sms-service.ts\");\n\nconst express = __webpack_require__(/*! express */ \"express\");\nconst app = express();\nconst serverless = __webpack_require__(/*! serverless-http */ \"serverless-http\");\nconst handler = serverless(app, {\n    request: async function (request) {\n    },\n});\napp.use('/', async (req, res) => {\n    let event = Buffer.isBuffer(req.body) ? JSON.parse(req.body.toString()) : req.body;\n    let status = 200;\n    let createEndPointResponse = null;\n    try {\n        const valNumData = await new _service_sms_service__WEBPACK_IMPORTED_MODULE_0__[\"SMSService\"]().validateNumber(event);\n        if (valNumData['NumberValidateResponse']['PhoneTypeCode'] == 0) {\n            event.validatedNumData = valNumData;\n            createEndPointResponse = await new _service_sms_service__WEBPACK_IMPORTED_MODULE_0__[\"SMSService\"]().createEndpoint(event);\n        }\n        res.status(status).send(createEndPointResponse);\n    }\n    catch (err) {\n        res.status(500).send(err);\n    }\n});\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvZnVuY3Rpb24vcG9zdEVuZHBvaW50L2luZGV4LnRzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vc3JjL2Z1bmN0aW9uL3Bvc3RFbmRwb2ludC9pbmRleC50cz9mMWZlIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFNNU1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlL3Ntcy1zZXJ2aWNlJztcbmNvbnN0IGV4cHJlc3MgPSByZXF1aXJlKCdleHByZXNzJyk7XG5leHBvcnQgY29uc3QgYXBwID0gZXhwcmVzcygpO1xuY29uc3Qgc2VydmVybGVzcyA9IHJlcXVpcmUoJ3NlcnZlcmxlc3MtaHR0cCcpO1xuXG5leHBvcnQgY29uc3QgaGFuZGxlciA9IHNlcnZlcmxlc3MoYXBwLCB7XG4gIHJlcXVlc3Q6IGFzeW5jIGZ1bmN0aW9uIChyZXF1ZXN0OiBhbnkpIHtcbiAgfSxcbn0pO1xuXG5hcHAudXNlKCcvJywgYXN5bmMgKHJlcTogYW55LCByZXM6IGFueSkgPT4ge1xuICAvLyBjb25zb2xlLmxvZygnUmVxdWVzdDogJywgcmVxLnBhcmFtcyk7XG4gIGxldCBldmVudCA9IEJ1ZmZlci5pc0J1ZmZlcihyZXEuYm9keSkgPyBKU09OLnBhcnNlKHJlcS5ib2R5LnRvU3RyaW5nKCkpIDogcmVxLmJvZHk7XG4gIGxldCBzdGF0dXM6IG51bWJlciA9IDIwMDtcbiAgbGV0IGNyZWF0ZUVuZFBvaW50UmVzcG9uc2U6IGFueSA9IG51bGw7XG4gIHRyeSB7XG4gICAgY29uc3QgdmFsTnVtRGF0YSA9IGF3YWl0IG5ldyBTTVNTZXJ2aWNlKCkudmFsaWRhdGVOdW1iZXIoZXZlbnQpO1xuICAgIGlmICh2YWxOdW1EYXRhWydOdW1iZXJWYWxpZGF0ZVJlc3BvbnNlJ11bJ1Bob25lVHlwZUNvZGUnXSA9PSAwKSB7XG4gICAgICBldmVudC52YWxpZGF0ZWROdW1EYXRhID0gdmFsTnVtRGF0YTtcbiAgICAgIGNyZWF0ZUVuZFBvaW50UmVzcG9uc2UgPSBhd2FpdCBuZXcgU01TU2VydmljZSgpLmNyZWF0ZUVuZHBvaW50KGV2ZW50KTtcbiAgICB9XG4gICAgcmVzLnN0YXR1cyhzdGF0dXMpLnNlbmQoY3JlYXRlRW5kUG9pbnRSZXNwb25zZSk7XG4gIH0gY2F0Y2ggKGVycikge1xuICAgIHJlcy5zdGF0dXMoNTAwKS5zZW5kKGVycik7XG4gIH1cbn0pO1xuIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTsiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/function/postEndpoint/index.ts\n");

/***/ }),

/***/ "./src/service/sms-service.ts":
/*!************************************!*\
  !*** ./src/service/sms-service.ts ***!
  \************************************/
/*! exports provided: SMSService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"SMSService\", function() { return SMSService; });\nconst AWS = __webpack_require__(/*! aws-sdk */ \"aws-sdk\");\nclass SMSService {\n    constructor() {\n        this.originationNumber = process.env.ORIGINATION_NUMBER;\n        this.pinpoint = new AWS.Pinpoint({ region: process.env.region });\n    }\n    async postSMS(event) {\n        const params = {\n            ApplicationId: event.appId,\n            MessageRequest: {\n                Addresses: {\n                    [event.destinationNumber]: {\n                        ChannelType: 'SMS'\n                    }\n                },\n                MessageConfiguration: {\n                    SMSMessage: {\n                        Body: event.message,\n                        MessageType: event.messageType,\n                    }\n                }\n            }\n        };\n        const response = await new Promise((resolve, reject) => {\n            this.pinpoint.sendMessages(params, function (err, res) {\n                if (err) {\n                    console.log(err.message);\n                    return reject(err);\n                }\n                else {\n                    console.log(JSON.stringify(res));\n                    console.log(\"Message sent! \" + res.MessageResponse.Result[event.destinationNumber].StatusMessage);\n                    return resolve(res);\n                }\n            });\n        });\n        return response;\n    }\n    async validateNumber(event) {\n        let destinationNumber = event.destinationNumber;\n        if (destinationNumber.length == 10) {\n            destinationNumber = \"+1\" + destinationNumber;\n        }\n        const params = {\n            NumberValidateRequest: {\n                IsoCountryCode: 'US',\n                PhoneNumber: destinationNumber\n            }\n        };\n        const response = await new Promise((resolve, reject) => {\n            this.pinpoint.phoneNumberValidate(params, function (err, data) {\n                if (err) {\n                    console.log(err, err.stack);\n                    return reject(err);\n                }\n                else {\n                    console.log(JSON.stringify(data));\n                    if (data['NumberValidateResponse']['PhoneTypeCode'] == 0) {\n                        return resolve(data);\n                    }\n                    else {\n                        const error = {\n                            status: 400,\n                            message: \"Received a phone number that isn't capable of receiving SMS messages. No endpoint created.\",\n                        };\n                        console.log(\"Received a phone number that isn't capable of receiving SMS messages. No endpoint created.\");\n                        return reject(error);\n                    }\n                }\n            });\n        });\n        return response;\n    }\n    async createEndpoint(event) {\n        const destinationNumber = event.validatedNumData['NumberValidateResponse']['CleansedPhoneNumberE164'];\n        const endpointId = event.validatedNumData['NumberValidateResponse']['CleansedPhoneNumberE164'].substring(1);\n        const params = {\n            ApplicationId: event.appId,\n            EndpointId: endpointId,\n            EndpointRequest: {\n                ChannelType: 'SMS',\n                Address: destinationNumber,\n                OptOut: 'ALL',\n                Location: {\n                    PostalCode: event.validatedNumData['NumberValidateResponse']['ZipCode'],\n                    City: event.validatedNumData['NumberValidateResponse']['City'],\n                    Country: event.validatedNumData['NumberValidateResponse']['CountryCodeIso2'],\n                },\n                Demographic: {\n                    Timezone: event.validatedNumData['NumberValidateResponse']['Timezone']\n                },\n                Attributes: {\n                    Source: [\n                        event.source\n                    ]\n                },\n                User: {\n                    UserAttributes: {\n                        FirstName: [\n                            event.firstName\n                        ],\n                        LastName: [\n                            event.lastName\n                        ]\n                    }\n                }\n            }\n        };\n        const response = await new Promise((resolve, reject) => {\n            this.pinpoint.updateEndpoint(params, function (err, data) {\n                if (err) {\n                    console.log(err, err.stack);\n                    return reject(err);\n                }\n                else {\n                    console.log(data);\n                    return resolve(data);\n                }\n            });\n        });\n        return response;\n    }\n    async updateEndpoint(event) {\n        var endpointId = event.originationNumber.substring(1);\n        const params = {\n            ApplicationId: event.appId,\n            EndpointId: endpointId,\n            EndpointRequest: {\n                Address: this.originationNumber,\n                ChannelType: 'SMS',\n                OptOut: 'NONE',\n                Attributes: {\n                    OptInTimestamp: [\n                        event.timestamp\n                    ]\n                },\n            }\n        };\n        const response = await new Promise((resolve, reject) => {\n            this.pinpoint.updateEndpoint(params, function (err, data) {\n                if (err) {\n                    console.log(\"An error occurred.\\n\");\n                    console.log(err, err.stack);\n                    return reject(err);\n                }\n                else {\n                    console.log(\"Successfully changed the opt status of endpoint ID \" + endpointId);\n                    return resolve(data);\n                }\n            });\n        });\n        return response;\n    }\n}\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvc2VydmljZS9zbXMtc2VydmljZS50cy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3NyYy9zZXJ2aWNlL3Ntcy1zZXJ2aWNlLnRzPzNhYmMiXSwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgQVdTID0gcmVxdWlyZSgnYXdzLXNkaycpO1xuXG5leHBvcnQgY2xhc3MgU01TU2VydmljZSB7XG4gIC8vIHByaXZhdGUgYXdzX3JlZ2lvbjpzdHJpbmcgPSBcInVzLWVhc3QtMVwiO1xuICAvLyBwcml2YXRlIG9yaWdpbmF0aW9uTnVtYmVyOiBzdHJpbmcgPSBcIisxMjA2NTU1MDE5OVwiO1xuICAvLyBUaGUgcGhvbmUgbnVtYmVyIG9yIHNob3J0IGNvZGUgdG8gc2VuZCB0aGUgbWVzc2FnZSBmcm9tLiBUaGUgcGhvbmUgbnVtYmVyXG4gIC8vIG9yIHNob3J0IGNvZGUgdGhhdCB5b3Ugc3BlY2lmeSBoYXMgdG8gYmUgYXNzb2NpYXRlZCB3aXRoIHlvdXIgQW1hem9uIFBpbnBvaW50XG4gIC8vIGFjY291bnQuIEZvciBiZXN0IHJlc3VsdHMsIHNwZWNpZnkgbG9uZyBjb2RlcyBpbiBFLjE2NCBmb3JtYXQuXG4gIC8vIHByaXZhdGUgZGVzdGluYXRpb25OdW1iZXI6IHN0cmluZyA9IFwiKzE3MDMtNDc0LTU5NjJcIjtcbiAgLyogcHJpdmF0ZSBtZXNzYWdlOiBzdHJpbmcgPSBcIlRoaXMgbWVzc2FnZSB3YXMgc2VudCB0aHJvdWdoIEFtYXpvbiBQaW5wb2ludCBcIlxuICAgKyBcInVzaW5nIHRoZSBBV1MgU0RLIGZvciBKYXZhU2NyaXB0IGluIE5vZGUuanMuIFJlcGx5IFNUT1AgdG8gXCJcbiAgICsgXCJvcHQgb3V0LlwiOyAqL1xuICAvLyBwcml2YXRlIG1lc3NhZ2U6IHN0cmluZyA9IFwiUGlucG9pbnQgc21zIHRlc3RcIjtcbiAgLy8gcHJpdmF0ZSBhcHBsaWNhdGlvbklkID0gXCJmOGExODU2Y2UzYjc0ODJjOTBhM2QzOWRmMTI4Y2VjOVwiO1xuICAvLyBwcml2YXRlIG1lc3NhZ2VUeXBlOiBzdHJpbmcgPSBcIlRSQU5TQUNUSU9OQUxcIjtcbiAgLy8gcHJpdmF0ZSByZWdpc3RlcmVkS2V5d29yZDpzdHJpbmcgPSBcIm15S2V5d29yZFwiO1xuICAvLyBwcml2YXRlIHNlbmRlcklkOnN0cmluZyA9IFwiTXlTZW5kZXJJRFwiO1xuICBwcml2YXRlIHBpbnBvaW50OiBhbnk7XG4gIHByaXZhdGUgb3JpZ2luYXRpb25OdW1iZXI6IHN0cmluZyA9IHByb2Nlc3MuZW52Lk9SSUdJTkFUSU9OX05VTUJFUjtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLnBpbnBvaW50ID0gbmV3IEFXUy5QaW5wb2ludCh7IHJlZ2lvbjogcHJvY2Vzcy5lbnYucmVnaW9uIH0pO1xuICB9XG4gIHB1YmxpYyBhc3luYyBwb3N0U01TKGV2ZW50OiBhbnkpOiBQcm9taXNlPGFueT4ge1xuICAgIGNvbnN0IHBhcmFtczogYW55ID0ge1xuICAgICAgQXBwbGljYXRpb25JZDogZXZlbnQuYXBwSWQsXG4gICAgICBNZXNzYWdlUmVxdWVzdDoge1xuICAgICAgICBBZGRyZXNzZXM6IHtcbiAgICAgICAgICBbZXZlbnQuZGVzdGluYXRpb25OdW1iZXJdOiB7XG4gICAgICAgICAgICBDaGFubmVsVHlwZTogJ1NNUydcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIE1lc3NhZ2VDb25maWd1cmF0aW9uOiB7XG4gICAgICAgICAgU01TTWVzc2FnZToge1xuICAgICAgICAgICAgQm9keTogZXZlbnQubWVzc2FnZSxcbiAgICAgICAgICAgIE1lc3NhZ2VUeXBlOiBldmVudC5tZXNzYWdlVHlwZSxcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9O1xuXG4gICAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCBuZXcgUHJvbWlzZSgocmVzb2x2ZTogYW55LCByZWplY3Q6IGFueSkgPT4ge1xuICAgICAgdGhpcy5waW5wb2ludC5zZW5kTWVzc2FnZXMocGFyYW1zLCBmdW5jdGlvbiAoZXJyLCByZXMpIHtcbiAgICAgICAgaWYgKGVycikge1xuICAgICAgICAgIGNvbnNvbGUubG9nKGVyci5tZXNzYWdlKTtcbiAgICAgICAgICByZXR1cm4gcmVqZWN0KGVycik7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgY29uc29sZS5sb2coSlNPTi5zdHJpbmdpZnkocmVzKSk7XG4gICAgICAgICAgY29uc29sZS5sb2coXCJNZXNzYWdlIHNlbnQhIFwiICsgcmVzLk1lc3NhZ2VSZXNwb25zZS5SZXN1bHRbZXZlbnQuZGVzdGluYXRpb25OdW1iZXJdLlN0YXR1c01lc3NhZ2UpO1xuICAgICAgICAgIHJldHVybiByZXNvbHZlKHJlcyk7XG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgfSk7XG4gICAgcmV0dXJuIHJlc3BvbnNlO1xuICB9XG5cbiAgcHVibGljIGFzeW5jIHZhbGlkYXRlTnVtYmVyKGV2ZW50OiBhbnkpOiBQcm9taXNlPGFueT4ge1xuICAgIGxldCBkZXN0aW5hdGlvbk51bWJlcjogU3RyaW5nID0gZXZlbnQuZGVzdGluYXRpb25OdW1iZXI7XG4gICAgaWYgKGRlc3RpbmF0aW9uTnVtYmVyLmxlbmd0aCA9PSAxMCkge1xuICAgICAgZGVzdGluYXRpb25OdW1iZXIgPSBcIisxXCIgKyBkZXN0aW5hdGlvbk51bWJlcjtcbiAgICB9XG4gICAgY29uc3QgcGFyYW1zID0ge1xuICAgICAgTnVtYmVyVmFsaWRhdGVSZXF1ZXN0OiB7XG4gICAgICAgIElzb0NvdW50cnlDb2RlOiAnVVMnLFxuICAgICAgICBQaG9uZU51bWJlcjogZGVzdGluYXRpb25OdW1iZXJcbiAgICAgIH1cbiAgICB9O1xuICAgIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgbmV3IFByb21pc2UoKHJlc29sdmU6IGFueSwgcmVqZWN0OiBhbnkpID0+IHtcbiAgICAgIHRoaXMucGlucG9pbnQucGhvbmVOdW1iZXJWYWxpZGF0ZShwYXJhbXMsIGZ1bmN0aW9uIChlcnIsIGRhdGEpIHtcbiAgICAgICAgaWYgKGVycikge1xuICAgICAgICAgIGNvbnNvbGUubG9nKGVyciwgZXJyLnN0YWNrKTtcbiAgICAgICAgICByZXR1cm4gcmVqZWN0KGVycik7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgY29uc29sZS5sb2coSlNPTi5zdHJpbmdpZnkoZGF0YSkpO1xuICAgICAgICAgIC8vcmV0dXJuIGRhdGE7XG4gICAgICAgICAgaWYgKGRhdGFbJ051bWJlclZhbGlkYXRlUmVzcG9uc2UnXVsnUGhvbmVUeXBlQ29kZSddID09IDApIHtcbiAgICAgICAgICAgIHJldHVybiByZXNvbHZlKGRhdGEpO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjb25zdCBlcnJvcjogYW55ID0ge1xuICAgICAgICAgICAgICBzdGF0dXM6IDQwMCxcbiAgICAgICAgICAgICAgbWVzc2FnZTogXCJSZWNlaXZlZCBhIHBob25lIG51bWJlciB0aGF0IGlzbid0IGNhcGFibGUgb2YgcmVjZWl2aW5nIFNNUyBtZXNzYWdlcy4gTm8gZW5kcG9pbnQgY3JlYXRlZC5cIixcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIlJlY2VpdmVkIGEgcGhvbmUgbnVtYmVyIHRoYXQgaXNuJ3QgY2FwYWJsZSBvZiByZWNlaXZpbmcgU01TIG1lc3NhZ2VzLiBObyBlbmRwb2ludCBjcmVhdGVkLlwiKTtcbiAgICAgICAgICAgIHJldHVybiByZWplY3QoZXJyb3IpO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSlcbiAgICB9KTtcbiAgICByZXR1cm4gcmVzcG9uc2U7XG4gIH1cblxuICBwdWJsaWMgYXN5bmMgY3JlYXRlRW5kcG9pbnQoZXZlbnQ6IGFueSk6IFByb21pc2U8YW55PiB7XG4gICAgY29uc3QgZGVzdGluYXRpb25OdW1iZXIgPSBldmVudC52YWxpZGF0ZWROdW1EYXRhWydOdW1iZXJWYWxpZGF0ZVJlc3BvbnNlJ11bJ0NsZWFuc2VkUGhvbmVOdW1iZXJFMTY0J107XG4gICAgY29uc3QgZW5kcG9pbnRJZCA9IGV2ZW50LnZhbGlkYXRlZE51bURhdGFbJ051bWJlclZhbGlkYXRlUmVzcG9uc2UnXVsnQ2xlYW5zZWRQaG9uZU51bWJlckUxNjQnXS5zdWJzdHJpbmcoMSk7XG4gICAgY29uc3QgcGFyYW1zOiBhbnkgPSB7XG4gICAgICBBcHBsaWNhdGlvbklkOiBldmVudC5hcHBJZCxcbiAgICAgIC8vIFRoZSBFbmRwb2ludCBJRCBpcyBlcXVhbCB0byB0aGUgY2xlYW5zZWQgcGhvbmUgbnVtYmVyIG1pbnVzIHRoZSBsZWFkaW5nXG4gICAgICAvLyBwbHVzIHNpZ24uIFRoaXMgbWFrZXMgaXQgZWFzaWVyIHRvIGVhc2lseSB1cGRhdGUgdGhlIGVuZHBvaW50IGxhdGVyLlxuICAgICAgRW5kcG9pbnRJZDogZW5kcG9pbnRJZCxcbiAgICAgIEVuZHBvaW50UmVxdWVzdDoge1xuICAgICAgICBDaGFubmVsVHlwZTogJ1NNUycsXG4gICAgICAgIEFkZHJlc3M6IGRlc3RpbmF0aW9uTnVtYmVyLFxuICAgICAgICAvLyBPcHRPdXQgaXMgc2V0IHRvIEFMTCAodGhhdCBpcywgZW5kcG9pbnQgaXMgb3B0ZWQgb3V0IG9mIGFsbCBtZXNzYWdlcylcbiAgICAgICAgLy8gYmVjYXVzZSB0aGUgcmVjaXBpZW50IGhhc24ndCBjb25maXJtZWQgdGhlaXIgc3Vic2NyaXB0aW9uIGF0IHRoaXNcbiAgICAgICAgLy8gcG9pbnQuIFdoZW4gdGhleSBjb25maXJtLCBhIGRpZmZlcmVudCBMYW1iZGEgZnVuY3Rpb24gY2hhbmdlcyB0aGlzIFxuICAgICAgICAvLyB2YWx1ZSB0byBOT05FIChub3Qgb3B0ZWQgb3V0KS5cbiAgICAgICAgT3B0T3V0OiAnQUxMJyxcbiAgICAgICAgTG9jYXRpb246IHtcbiAgICAgICAgICBQb3N0YWxDb2RlOiBldmVudC52YWxpZGF0ZWROdW1EYXRhWydOdW1iZXJWYWxpZGF0ZVJlc3BvbnNlJ11bJ1ppcENvZGUnXSxcbiAgICAgICAgICBDaXR5OiBldmVudC52YWxpZGF0ZWROdW1EYXRhWydOdW1iZXJWYWxpZGF0ZVJlc3BvbnNlJ11bJ0NpdHknXSxcbiAgICAgICAgICBDb3VudHJ5OiBldmVudC52YWxpZGF0ZWROdW1EYXRhWydOdW1iZXJWYWxpZGF0ZVJlc3BvbnNlJ11bJ0NvdW50cnlDb2RlSXNvMiddLFxuICAgICAgICB9LFxuICAgICAgICBEZW1vZ3JhcGhpYzoge1xuICAgICAgICAgIFRpbWV6b25lOiBldmVudC52YWxpZGF0ZWROdW1EYXRhWydOdW1iZXJWYWxpZGF0ZVJlc3BvbnNlJ11bJ1RpbWV6b25lJ11cbiAgICAgICAgfSxcbiAgICAgICAgQXR0cmlidXRlczoge1xuICAgICAgICAgIFNvdXJjZTogW1xuICAgICAgICAgICAgZXZlbnQuc291cmNlXG4gICAgICAgICAgXVxuICAgICAgICB9LFxuICAgICAgICBVc2VyOiB7XG4gICAgICAgICAgVXNlckF0dHJpYnV0ZXM6IHtcbiAgICAgICAgICAgIEZpcnN0TmFtZTogW1xuICAgICAgICAgICAgICBldmVudC5maXJzdE5hbWVcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBMYXN0TmFtZTogW1xuICAgICAgICAgICAgICBldmVudC5sYXN0TmFtZVxuICAgICAgICAgICAgXVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH07XG4gICAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCBuZXcgUHJvbWlzZSgocmVzb2x2ZTogYW55LCByZWplY3Q6IGFueSkgPT4ge1xuICAgICAgdGhpcy5waW5wb2ludC51cGRhdGVFbmRwb2ludChwYXJhbXMsIGZ1bmN0aW9uIChlcnIsIGRhdGEpIHtcbiAgICAgICAgaWYgKGVycikge1xuICAgICAgICAgIGNvbnNvbGUubG9nKGVyciwgZXJyLnN0YWNrKTtcbiAgICAgICAgICByZXR1cm4gcmVqZWN0KGVycik7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgY29uc29sZS5sb2coZGF0YSk7XG4gICAgICAgICAgLy8gdGhpcy5wb3N0U01TKGRlc3RpbmF0aW9uTnVtYmVyKTtcbiAgICAgICAgICByZXR1cm4gcmVzb2x2ZShkYXRhKTtcbiAgICAgICAgfVxuICAgICAgfSlcbiAgICB9KTtcbiAgICByZXR1cm4gcmVzcG9uc2U7XG4gIH1cblxuICBwdWJsaWMgYXN5bmMgdXBkYXRlRW5kcG9pbnQoZXZlbnQ6IGFueSk6IFByb21pc2U8YW55PiB7XG4gICAgdmFyIGVuZHBvaW50SWQgPSBldmVudC5vcmlnaW5hdGlvbk51bWJlci5zdWJzdHJpbmcoMSk7XG5cbiAgICBjb25zdCBwYXJhbXM6IGFueSA9IHtcbiAgICAgIEFwcGxpY2F0aW9uSWQ6IGV2ZW50LmFwcElkLFxuICAgICAgRW5kcG9pbnRJZDogZW5kcG9pbnRJZCxcbiAgICAgIEVuZHBvaW50UmVxdWVzdDoge1xuICAgICAgICBBZGRyZXNzOiB0aGlzLm9yaWdpbmF0aW9uTnVtYmVyLFxuICAgICAgICBDaGFubmVsVHlwZTogJ1NNUycsXG4gICAgICAgIE9wdE91dDogJ05PTkUnLFxuICAgICAgICBBdHRyaWJ1dGVzOiB7XG4gICAgICAgICAgT3B0SW5UaW1lc3RhbXA6IFtcbiAgICAgICAgICAgIGV2ZW50LnRpbWVzdGFtcFxuICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgIH1cbiAgICB9O1xuICAgIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgbmV3IFByb21pc2UoKHJlc29sdmU6IGFueSwgcmVqZWN0OiBhbnkpID0+IHtcbiAgICAgIHRoaXMucGlucG9pbnQudXBkYXRlRW5kcG9pbnQocGFyYW1zLCBmdW5jdGlvbiAoZXJyLCBkYXRhKSB7XG4gICAgICAgIGlmIChlcnIpIHtcbiAgICAgICAgICBjb25zb2xlLmxvZyhcIkFuIGVycm9yIG9jY3VycmVkLlxcblwiKTtcbiAgICAgICAgICBjb25zb2xlLmxvZyhlcnIsIGVyci5zdGFjayk7XG4gICAgICAgICAgcmV0dXJuIHJlamVjdChlcnIpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIGNvbnNvbGUubG9nKFwiU3VjY2Vzc2Z1bGx5IGNoYW5nZWQgdGhlIG9wdCBzdGF0dXMgb2YgZW5kcG9pbnQgSUQgXCIgKyBlbmRwb2ludElkKTtcbiAgICAgICAgICByZXR1cm4gcmVzb2x2ZShkYXRhKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfSk7XG4gICAgcmV0dXJuIHJlc3BvbnNlO1xuICB9XG5cbn1cblxuXG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUVBO0FBa0JBO0FBRkE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Iiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/service/sms-service.ts\n");

/***/ }),

/***/ "aws-sdk":
/*!**************************!*\
  !*** external "aws-sdk" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"aws-sdk\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXdzLXNkay5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9leHRlcm5hbCBcImF3cy1zZGtcIj81MTQyIl0sInNvdXJjZXNDb250ZW50IjpbIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImF3cy1zZGtcIik7Il0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///aws-sdk\n");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"express\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXhwcmVzcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9leHRlcm5hbCBcImV4cHJlc3NcIj8yMmZlIl0sInNvdXJjZXNDb250ZW50IjpbIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImV4cHJlc3NcIik7Il0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///express\n");

/***/ }),

/***/ "serverless-http":
/*!**********************************!*\
  !*** external "serverless-http" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"serverless-http\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VydmVybGVzcy1odHRwLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwic2VydmVybGVzcy1odHRwXCI/N2Y3ZSJdLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJzZXJ2ZXJsZXNzLWh0dHBcIik7Il0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///serverless-http\n");

/***/ })

/******/ })));